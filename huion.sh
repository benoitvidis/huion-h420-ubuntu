#!/bin/bash

pad='HUION H420 Pad pad'
stylus='HUION H420 Pen stylus'

monitor=VGA-1

# rotate 
xsetwacom --set "$stylus" Rotate half

# pad buttons
xsetwacom --set "$pad" Button 1 "key h"
xsetwacom --set "$pad" Button 2 "key e"
xsetwacom --set "$pad" Button 3 "key +ctrl +z -z -ctrl"


if xrandr --listactivemonitors | grep $monitor; then
	pen_id=$(xinput | grep "H420 Pen" | sed -r 's|^.*id=([0-9]+).*|\1|')
	xinput map-to-output $pen_id $monitor
fi

exit 0
